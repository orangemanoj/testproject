package bi.framework.design;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import bl.framework.api.SeleniumBase;

public class ProjectMethods extends SeleniumBase {
	
	public static String tcDesc,tcName,author,category, dataSheetName;
 
	@BeforeSuite
	//@BeforeSuite(groups="Common")
	public void beforeSuite()
	{
		startReport();
	}
	
	@AfterSuite
	//@AfterSuite(groups="Common")
	public void afterSuite()
	{
		endReport();
	}
	
	@BeforeClass
	//@BeforeClass(groups="Common")
	public void beforeClass()
	{
		InitializeTest(tcName, tcDesc, author, category);
	}
	
	@Parameters({"url","username","pwd"})
	@BeforeMethod
	//@BeforeMethod(groups="Common")
	public void login(String url, String uname, String pwd){
		        //Launch Browser and enter the url
				//startApp("Chrome","http://leaftaps.com/opentaps");
		         startApp("Chrome",url);
				//Enter UserName
				WebElement username = locateElement("id", "username");
				//clearAndType(username, "DemoSalesManager");
				clearAndType(username, uname);
				
				//Enter Password
				WebElement password = locateElement("id", "password");
				//clearAndType(password, "crmsfa");
				clearAndType(password, pwd);
				
				//Click Login
				WebElement login = locateElement("classname", "decorativeSubmit");
				click(login);
				
				//Click CRMSFA Link
				WebElement crmsfalink = locateElement("linktext","CRM/SFA");
				click(crmsfalink);
				
				//Click Leads
				WebElement leads = locateElement("linktext","Leads");
				click(leads);
	}
	
	@AfterMethod
	//@AfterMethod(groups="Common")
	public void closeApp() {
		close();
	}	
	
	public void clickwithoutsnap(WebElement ele) {
		try {
			ele.click();
			System.out.println("The element clicked successfully");
			
		} catch (UnknownError e) {
		
			System.out.println("No such element to click");
		}
	}
		
	@DataProvider (name = "fetchData")
	public object[][] getData(){
		return ReadExcel
				.readData(dataSheetName);
			
	}
		
		
	}
	
}





package testCaseBasic;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class TcCreateLead {

	public static void main(String[] args) {
			//Set Property 
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		//Launch Browser
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
			
		//Locators - text fields
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByPartialLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("XYZ pvt ltd");
		driver.findElementById("createLeadForm_firstName").sendKeys("Manoj");
		driver.findElementById("createLeadForm_lastName").sendKeys("Amara");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("Manojkumar@gmail.com");
		WebElement lstOwn = driver.findElementById("createLeadForm_ownershipEnumId");
		Select selOwn = new Select(lstOwn);
		selOwn.selectByVisibleText("Sole Proprietorship");
		
		
		// Locators - Drop down
			//1.Find WebElement
			//2.Convert WebElement into Select(since there is no direct methods in webElement like click,sendKeys etc)
			//3.Convert Select into List(to get the size of the drop down value).
					//3.1 use SelectBy index method.
			//4.use SelectBy Methods.
			
				//Locators - drop down - select value directly
		WebElement lstSource = driver.findElementById("createLeadForm_dataSourceId");
		Select selSource = new Select(lstSource);
		selSource.selectByVisibleText("Word of Mouth");
		
				//Locators - drop down - select value by index
		WebElement lstMarket = driver.findElementById("createLeadForm_marketingCampaignId");
		Select selMarket = new Select(lstMarket);
		selMarket.selectByIndex(2);
		
				//Locators - drop down - select value by condition using List(to get the size of the list values) 
		WebElement lstIndustry = driver.findElementById("createLeadForm_industryEnumId");
		Select selIndustry = new Select(lstIndustry);
		List<WebElement> objOptions = selIndustry.getOptions();
		objOptions.size();
		System.out.println(objOptions.size());
		selIndustry.selectByIndex(objOptions.size()- 4 );
		
				//Locators - drop down - 
		WebElement lstCurrency = driver.findElementById("createLeadForm_currencyUomId");
		Select selCurrency = new Select(lstCurrency);
		selCurrency.selectByVisibleText("INR - Indian Rupee");
		driver.findElementByName("submitButton").click();
		
//		List<WebElement> objOptCur = selCurrency.getOptions();
//		objOptCur.size();
//		for (int i = 0; i < objOptCur.size(); i++ ) {
//			if (objOptCur.
//		}
				
		// Verification of Create lead.
				//1.Create WebElement for the required text field.
				//2.Get text from the above WebElement
				//3.Verify text in WebElement.
		WebElement elCompanyName = driver.findElementById("viewLead_companyName_sp");
		String strCompanyName = elCompanyName.getText();
		if(strCompanyName.contains("XYZ pvt ltd"))
		{
			System.out.println("The given company name has been created & Verified.");
		}
		else
		{
			System.out.println("The given company name is not found");
		}
		
	}

}

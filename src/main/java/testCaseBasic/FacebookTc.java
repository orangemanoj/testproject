package testCaseBasic;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class FacebookTc {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeOptions op = new ChromeOptions();
		op.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(op);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.facebook.com/");
		driver.findElementById("email").sendKeys("manojsfriends2000@gmail.com");
		driver.findElementById("pass").sendKeys("AVV1manoj2kumar3$$");
		driver.findElementById("loginbutton").click();
		driver.findElementByClassName("_1frb").sendKeys("TestLeaf");
		driver.findElementByXPath("(//button[@type = 'submit'])[1]").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//li[@role = 'presentation'])[8]").click();
		String text = driver.findElementByXPath("//a[contains(text(), 'TestLeaf')]").getText();
		System.out.println("Text is verified  " + text);
		String textLike = driver.findElementByXPath("(//button[@type = 'submit'])[4]").getText();
		System.out.println(textLike);
		if (textLike.equals("Like")){
			driver.findElementByXPath("(//button[@type = 'submit'])[4]").click();
		} 
		else {
			System.out.println("The page is already liked");
		}
		driver.findElementByXPath("//a[contains(text(),'TestLeaf')]").click();
		String actualTitle = driver.getTitle();
		System.out.println("The actual title of the page is     "   +   actualTitle);	
		String expectedTitle = "TestLeaf";
		if(actualTitle .contains(expectedTitle)) {
			System.out.println("Title verified");
		}
		else {
			System.out.println("Title didnt match");	
			}	
		String Like = driver.findElementByXPath("(//div[@class = 'clearfix _ikh'])[2]//div[2]").getText();
		System.out.println("Total Likes of this page      " +  Like);
		driver.close();
		
	}
	
	}



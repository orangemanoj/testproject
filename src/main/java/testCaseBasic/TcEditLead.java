package testCaseBasic;


import static org.testng.Assert.assertEquals;

import org.openqa.selenium.chrome.ChromeDriver;

public class TcEditLead {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		driver.findElementById("username").sendKeys("DemosalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByPartialLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//input[@name='lastName'])[3]").sendKeys("Amara");
	//	driver.findElementByXPath("//input[@class = ' x-form-text x-form-field ']").sendKeys("Vihaan");
		driver.findElementByXPath("(//button[@class='x-btn-text'])[7]").click();
		Thread.sleep(1000);
		driver.findElementByXPath("//a[contains(text(),'10336')]").click();
		String actTitle = driver.getTitle();
		if (actTitle.contains("View Lead"))
		{
			System.out.println("Title verified");
		}
		else
		{
			System.out.println("title not match");	
		}
		driver.findElementByClassName("subMenuButton").click();
		driver.findElementByXPath("(//input[@name = 'companyName'])[2]").clear();
		driver.findElementByXPath("(//input[@name = 'companyName'])[2]").sendKeys("MEGAPOWER Co");
		driver.findElementByXPath("(//input[@name = 'submitButton'])[1]").click();
		driver.close();
		
	}

}

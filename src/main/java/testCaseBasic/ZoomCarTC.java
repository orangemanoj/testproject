package testCaseBasic;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.rules.Timeout;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ZoomCarTC {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.zoomcar.com/chennai/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.findElementByXPath("//a[@class = 'search']").click();
		driver.findElementByXPath("(//div[@class = 'items'])[1]").click();
		driver.findElementByXPath("//button[@class = 'proceed']").click();
		driver.findElementByXPath("//div[@class = 'days']//div[2]").click();
		Thread.sleep(2000);
//		// Get the current date
//		
//		Date date = new Date();
//
//		// Get only the date (and not month, year, time etc)
//				
//		DateFormat sdf = new SimpleDateFormat("dd");
//		 
//		// Get today's date
//				
//		String today = sdf.format(date);
//
//		// Convert to integer and add 1 to it
//		 
//		int tomorrow = Integer.parseInt(today)+1;
//
//		// Print tomorrow's date
				
		//System.out.println(tomorrow);
		driver.findElementByXPath("//button[@class = 'proceed']").click();
		driver.findElementByXPath("//div[@class = 'day picked low-price']").click();
		driver.findElementByXPath("//button[contains(text(),'Done')]").click();
		List<WebElement> resSize = driver.findElements(By.className("car-listing"));
		System.out.println("Total results are" + " " +resSize.size());
		Thread.sleep(3000);
		//driver.findElementByXPath("//div[contains(text(),' Price: High to Low ')]").click();
		List<WebElement> sort = driver.findElements(By.xpath("//div[@class='price']"));
		System.out.println(sort.size());
		String brandName = driver.findElementByXPath("(//div[@class ='details'])[1]//h3").getText();
		System.out.println("The Highest value Brand is   " + brandName);
		driver.findElementByName("book-now").click();
		driver.close();
		
		
		
	}

}

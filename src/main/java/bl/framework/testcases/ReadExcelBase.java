package bl.framework.testcases;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static void main(String[] args) throws IOException {
		XSSFWorkbook wbook = new XSSFWorkbook ("./Data/TC001.xlsx"); // Entering workbook
		XSSFSheet sheet = wbook.getSheet("Sheet1"); // Entering Sheet
		
		int rowCount = sheet.getLastRowNum(); // Getting total row count
		System.out.println("Total Row Count" + rowCount);
		
		int colCount = sheet.getRow(0).getLastCellNum(); // Getting Cell count for header row
		System.out.println("Total Column Count" + colCount);
		
			for (int i = 1; i <= rowCount; i++) {
				XSSFRow row =sheet.getRow(i);					//Entering row
				for (int j = 0; j < colCount; j++) {			//Entering cell
					XSSFCell cell = row.getCell(j);				//Getting value from Cell
					String text = cell.getStringCellValue();
					System.out.println(text);
					
				}
				
			}
		
		
		
		
	}

}

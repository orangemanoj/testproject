package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;




public class TC001_CreateLead extends bi.framework.design.ProjectMethods{
	Select sel; 
	
	@BeforeTest
	//@BeforeTest(groups="Smoke")
	public void setup() {
		tcName= "Create Lead";
		tcDesc= "Create Lead";
		author= "Laxmi Priya";
		category="Smoke";
	}

	@Test
	//@Test (/invocationCount=2 , invocationTimeOut=30000/)
	//@Test(groups="Smoke")
	public void createleads() {
		
		//login();
	
		//Click CreateLeads
		WebElement createlead = locateElement("linktext", "Create Lead");
		click(createlead);
		
		//Enter CompanyName
		WebElement companyname = locateElement("id", "createLeadForm_companyName");
		clearAndType(companyname, "Accenture");
		
		//Click and Switch to find parentAccount
		WebElement parentaccount = locateElement("xpath", "//input[@id = 'createLeadForm_parentPartyId']/following::a/./img");
		click(parentaccount);
		switchToWindow(1);
		
		//Enter Account ID
		WebElement accountid = locateElement("name", "id");
		clearAndType(accountid, "democlass1");
		
		//Click on Find Account
		WebElement findaccount = locateElement("xpath", "(//button[@class = 'x-btn-text'])[1]");
		click(findaccount);
	
		//Select the first value in the result
		WebElement result = locateElement("xpath", "(//div[@class = 'x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		String result1 = getElementText(result);
		//clickwithoutsnap(result);
		
		//Switch back to CreateLeads
		switchToWindow(0);
		
		//Enter parent account
		WebElement parentaccount1 = locateElement("id", "createLeadForm_parentPartyId");
		clearAndType(parentaccount1, result1);
		
		//Enter FirstName
		WebElement firstname = locateElement("id", "createLeadForm_firstName");
		clearAndType(firstname, "Lax");
		
		//Enter LastName
		WebElement lastname = locateElement("id", "createLeadForm_lastName");
		clearAndType(lastname, "Mahadevan");
		
		//Select Source from DropDown
		WebElement sourcelist = locateElement("createLeadForm_dataSourceId");
		selectDropDownUsingIndex(sourcelist, 1);
		
		//Select Marketing Campaign from DropDown
		WebElement marketinglist = locateElement("createLeadForm_marketingCampaignId");
		selectDropDownUsingText(marketinglist, "Automobile");
		
		//Enter FirstName Local
		WebElement firstnamelocal = locateElement("id", "createLeadForm_firstNameLocal");
		clearAndType(firstnamelocal, "lachu");
		
		//Enter LastName Local
		WebElement lastnamelocal = locateElement("id", "createLeadForm_lastNameLocal");
		clearAndType(lastnamelocal, "Mahadevan");
		
		//Enter Salutation
		WebElement salutation = locateElement("id", "createLeadForm_personalTitle");
		clearAndType(salutation, "Laxmi Salutation");
		
		//Enter BirthDate
		WebElement birthdate = locateElement("name", "birthDate");
		clearAndType(birthdate, "01/12/1990");
		
		//Enter Title
		WebElement title = locateElement("id", "createLeadForm_generalProfTitle");
		clearAndType(title, "Laxmi Profile");
		
		//Enter Department
		WebElement department = locateElement("id", "createLeadForm_departmentName");
		clearAndType(department, "Testing");
		
		//Enter AnnualRevenue
		WebElement annualrevenue = locateElement("id", "createLeadForm_annualRevenue");
		clearAndType(annualrevenue, "1000020");
		
		//Select PreferredCurrency from DropDown
		WebElement preferredcurrency = locateElement("createLeadForm_currencyUomId");
		selectDropDownUsingText(preferredcurrency, "AWG - Aruban Guilder");
	
		//Select Industry from DropDown
		WebElement industry = locateElement("createLeadForm_industryEnumId");
		selectDropDownUsingIndex(industry, 4);
		
		//Enter NoOfEmployees
		WebElement noofemployees = locateElement("id", "createLeadForm_numberEmployees");
		clearAndType(noofemployees, "100");
		
		//Enter Ownership from DropDown
		WebElement ownership = locateElement("createLeadForm_ownershipEnumId");
		selectDropDownUsingText(ownership, "Partnership");
		
		//Enter SICCode
		WebElement siccode = locateElement("id", "createLeadForm_sicCode");
		clearAndType(siccode, "12");
		
		//Enter TickerSymbol
		WebElement tickersymbol = locateElement("id", "createLeadForm_tickerSymbol");
		clearAndType(tickersymbol, "5236");
		
		//Enter description
		WebElement description = locateElement("id", "createLeadForm_description");
		clearAndType(description, "Testing Create Leads");
		
		//Enter Notes
		WebElement notes = locateElement("id", "createLeadForm_importantNote");
		clearAndType(notes, "Notes");
		
		WebElement email = locateElement("id", "createLeadForm_primaryEmail");
		clearAndType(email, "lax003@abc.com");
		
		//Click on Create Leads
		WebElement createsubmit = locateElement("name", "submitButton");
		click(createsubmit);
		
		//validate View page
		
		WebElement view = locateElement("id", "viewLead_firstName_sp");
		verifyDisplayed(view);
		
		//close the browser
		//close();
		
		@DataProvider(name = "getData1")
		public String[][] fetchdata(){
			String[][] data = new String[2][3];
			data[0][0] = "TestLeaf";
			data[0][1] = "Balaji";
			data[0][2] = "C";
			
			data[0][0] = "TestLeaf";
			data[0][1] = "Balaji";
			data[0][2] = "C";
			return data;
			
		}
		
		
		
	}
}
		
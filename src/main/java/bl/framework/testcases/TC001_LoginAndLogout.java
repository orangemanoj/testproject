package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class TC001_LoginAndLogout extends SeleniumBase{

	@Test
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager"); 
		
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa"); 
		
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); 
		
		WebElement crmsfa = locateElement ("partialLinkText","CRM/SFA");
		click(crmsfa);
		
		WebElement createLead = locateElement("LinkText","Create Lead");
		click(createLead);
		
		WebElement companyName = locateElement("id","createLeadForm_companyName");
		clearAndType(companyName, "ABC Pvt Ltd");
		
		WebElement firstName = locateElement ("id","createLeadForm_firstName");
		clearAndType(firstName,"Vihaan");
		
		WebElement lastName = locateElement ("id","createLeadForm_lastName");
		clearAndType(lastName,"Amara");

		
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		//click(eleLogout);
	

		
	}
}









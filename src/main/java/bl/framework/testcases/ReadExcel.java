package bl.framework.testcases;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static Object[][] readData(String dataSheetName, Object[][] data) throws IOException {
		XSSFWorkbook wbook;
		try {
		wbook = new XSSFWorkbook ("./Data/"+dataSheetName+".xlsx"); // Entering workbook
		XSSFSheet sheet = wbook.getSheet("Sheet1"); // Entering Sheet
		
		int rowCount = sheet.getLastRowNum(); // Getting total row count
		System.out.println("Total Row Count" + rowCount);
		
		int colCount = sheet.getRow(0).getLastCellNum(); // Getting Cell count for header row
		System.out.println("Total Column Count" + colCount);
		
		data = new Object[rowCount][colCount]; 				// create object
				
			for (int i = 1; i <= rowCount; i++) {
				XSSFRow row =sheet.getRow(i);					//Entering row
				for (int j = 0; j < colCount; j++) {			//Entering cell
					XSSFCell cell = row.getCell(j);				//Getting value from Cell
					data[i-1][j] = cell.getStringCellValue();
				}
			}
			} catch (IOException e){
				
			}
		return data;
		
		
		
		
	}

}

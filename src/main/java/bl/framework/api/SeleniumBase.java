package bl.framework.api;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import bl.framework.base.Browser;
import bl.framework.base.Element;

public class SeleniumBase extends ExtentReports1 implements Browser, Element {

	public RemoteWebDriver driver;
	public int i =1;
	@Override
	public void startApp(String url) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("The browser launched successfully");
        takeSnap();
	}

	@Override
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				//driver = new ChromeDriver();
				ChromeOptions op = new ChromeOptions();
				op.addArguments("--disable-notifications");
				 driver = new ChromeDriver(op);
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver(); 
			}
			driver.manage().window().maximize();
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			logStep("pass", "The Browser launched successfully");
			//System.out.println("The browser "+browser+" launched successfully");
		} catch (WebDriverException e) {
			logStep("fail", "The Browser launched successfully");
		}
        //takeSnap();
	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		try {
			switch (locatorType) {
			case "id": return driver.findElementById(value);
			case "name": return driver.findElementByName(value);
			case "classname": return driver.findElementByClassName(value);
			case "xpath": return driver.findElementByXPath(value);
			case "linktext": return driver.findElementByLinkText(value);
			default:
				break;
			}
		} catch (WebDriverException e) {
			
			logStep("fail","Unknown exception");
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {
		try {
			return driver.findElementById(value);
		} catch (NoSuchElementException e) {
			System.err.println("No such element exeception");
		}
		return null;
		
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void switchToAlert() {
		try {
			driver.switchTo().alert();
			
		} catch (NoAlertPresentException e) {
			System.err.println("No Alert present");
		}

	}

	@Override
	public void acceptAlert() {
		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();
			
		} catch (NoAlertPresentException e) {
			System.err.println("No Alert present");
		}

	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub

	}

	@Override
	public String getAlertText() {

		String alertText = getAlertText();		
		return alertText;
		
	}

	@Override
	public void typeAlert(String data) {
		

	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> windows = driver.getWindowHandles();
			List<String> ls = new ArrayList<>();
			ls.addAll(windows);
			System.out.println(ls);
			driver.switchTo().window(ls.get(index));
			//logStep("Pass", "Switched to Window Successfully");
			System.out.println("Switched to Window "+driver.getTitle()+" Successfully");
		} catch (NoSuchWindowException e) {
			//logStep("Fail", "No such window present");
			System.err.println("No such window present");
		}
	}

	@Override
	public void switchToWindow(String title) {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().window(title);
			System.out.println("Switched to "+title+" Successfully");
			//logStep("pass", "Switched to title successfully");
		} catch (NoSuchWindowException e) {
			System.err.println("No such window present");
		}
	}

	@Override
	public void switchToFrame(int index) {
		
		try {
			driver.switchTo().frame(index);
		} catch (NoSuchFrameException e) {
			//logStep("Fail", "No Such Frame presents");
		}

	}

	@Override
	public void switchToFrame(WebElement ele) {
		
		try {
			driver.switchTo().frame(ele);
			logStep("Pass", "Switched to Frame Successfully");
		} catch (NoSuchFrameException e) {
			logStep("Fail", "No Such Frame presents");
		}

	}

	@Override
	public void switchToFrame(String idOrName) {
		
		try {
			driver.switchTo().frame(idOrName);
			logStep("Pass", "Switched to Frame Successfully");
		} catch (NoSuchFrameException e) {
			logStep("Fail", "No Such Frame presents");
		}

	}

	@Override
	public void defaultContent() {
		try {
			driver.switchTo().defaultContent();
			logStep("Pass", "Switched to Frame Successfully");
		} catch (NoSuchFrameException e) {
			logStep("Fail", "No Such Frame presents");
		}
	}

	@Override
	public boolean verifyUrl(String url) {
		
		return false;
	}

	@Override
	public boolean verifyTitle(String title) {
		if (title.contains("Duplicate Lead")) {
			System.out.println("Duplicate lead title verified");
		}
		else if (title.contains("Edit Lead"))
		{
			System.out.println("Edit lead title verified");
		}
		return false;
	}

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void close() {
		driver.close();

	}

	@Override
	public void quit() {
		driver.quit();

	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			logStep("pass","The element clicked successfully");
			
		} catch (UnknownError e) {
		
			logStep("fail","No such element to click");
		}
		
	}

	@Override
	public void append(WebElement ele, String data) {
		String text = ele.getText();
		String append = text+data;
		System.out.println(append);
	}

	@Override
	public void clear(WebElement ele) {
		try {
			ele.clear();
			System.out.println("The data"+ele.getText()+ "Cleared");
		} catch (StaleElementReferenceException e) {
			System.out.println("Stale Element Exception");
		}
	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data); 
			logStep("pass","The data "+data+" entered successfully");
		} catch (WebDriverException e) {
			logStep("fail","The data "+data+" entered successfully");
		}
		
	}

	@Override
	
	public String getElementText(WebElement ele) {
		String text = ele.getText();
		System.out.println("The result "+text+" success");
		takeSnap();
		return text;
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTypedText(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		// TODO Auto-generated method stub
		try {
			Select sel = new Select(ele);
			sel.selectByVisibleText(value);
			takeSnap();
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			System.err.println("No such element exception is found");
		}
		
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
		try {
			Select sel = new Select(ele);
			sel.selectByIndex(index);
			takeSnap();
		} catch (NoSuchElementException e) {
			System.err.println("No such element exception is found");
		}
		
	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		// TODO Auto-generated method stub
		try {
			Select sel = new Select(ele);
			sel.selectByValue(value);
			takeSnap();
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			System.err.println("No such element exception is found");
		}
		
	}


	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		String text = ele.getText();
		System.out.println(text);
		if (text.equals(expectedText)) {
			System.out.println("Exact match");
		    takeSnap();
		}
		else System.out.println("Not Exact match");
		return false;
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		String text = ele.getText();
		if (text.contains(expectedText)) {
			System.out.println("Exact match");
		    takeSnap();
		}
		else System.out.println("Not Exact match");
		return false;
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		if (ele.isDisplayed()) {
			System.out.println("Lead created successfully");
			return true;
		}
		
		else System.out.println("Lead not created successfully");
		return false;
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

}
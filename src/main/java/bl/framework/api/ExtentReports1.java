package bl.framework.api;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReports1 {
	
	public static  ExtentHtmlReporter html;
	public static  ExtentReports extent;
	public ExtentTest test;
		
	public void startReport()
	{
		//create html with readable mode
		html = new ExtentHtmlReporter("./report/Report1.html");
		//create object for the report
		extent = new ExtentReports();
		html.setAppendExisting(true);
		//readable to writable
		extent.attachReporter(html);
	}
	
	public void InitializeTest(String tcName, String tcDesc, String author, String category)
	{
		//test case level
		test = extent.createTest(tcName, tcDesc);
		test.assignAuthor(author);
		test.assignCategory(category);
	}
		
	public void logStep(String status, String desc)
	{
		//test case step level
		if(status.equalsIgnoreCase("pass"))
			test.pass(desc);
		else if(status.equalsIgnoreCase("fail")) {
			test.fail(desc);
		throw new  RuntimeException();
		}
		else if(status.equalsIgnoreCase("warning"))
			test.warning(desc);		
	}
	
	public void endReport()
	{   
		//Mandatory for Extent Reports
		extent.flush();
	}

}